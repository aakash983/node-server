const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const PORT = process.env.PORT || 3002;

// Allow CORS (Cross-Origin Resource Sharing)
// app.use((req, res, next) => {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
//     res.header(
//         'Access-Control-Allow-Headers',
//         'Origin, X-Requested-With, Content-Type, Accept'
//     );
//     res.header('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0');
//     next();
// });
app.use(
  cors({
    origin: "*",
  })
);

// Your placeholder data
const posts = [
  {
    id: 1,
    serial: "T3mP9xL2uY8cJ4",
    review_status: "Open",
    sev: "High",
    name: 100.22,
    parameter: "name",
    site: "https://appex-reviewer.com/port:3001",
    path: "/testPortal:\\dashboard/tabContent/value",
    mitigation_status: "None",
    status: "unpublished",
  },
  {
    id: 2,
    serial: "W1nT3rSuMm3r2",
    review_status: "Valid",
    sev: "Medium",
    name: 101.22,
    parameter: "ParentId",
    site: "https://appex-reviewer.com/port:3001",
    path: "/testPortal:\\dashboard/tabContent/value",
    mitigation_status: "None",
    status: "unpublished",
  },
  {
    id: 3,
    serial: "SuW1nT3rMm3r2",
    review_status: "Valid",
    sev: "Low",
    name: 103.2,
    parameter: "ParentId",
    site: "https://appex-reviewer.com/port:3001",
    path: "/testPortal:\\dashboard/tabContent/value",
    mitigation_status: "None",
    status: "unpublished",
  },
  // Add more items as needed
];

const analysis = [
  {
    id: 1,
    serial: "b53jh2hjhh2lj2jh2e201",
    analysis_status: "Completed- Verifying Results",
    actual_start: "20 Jan 2024 4:18AM",
    actual_end: "20 Jan 2024 4:18AM",
    scheduled_start: "20 Jan 2024 4:18AM",
    scheduled_end: "20 Jan 2024 4:18AM",
    organization_id: 8832,
    organization_uuid: "c26tttvb-bvvvv22hh1jjj-fwwww-r333d",
    organization_name: "Optiv SAaas -LightBox",
  },
  {
    id: 2,
    serial: "c33rrhjh2hjhhkl22e2e9",
    analysis_status: "All Verification Failed",
    actual_start: "20 Jan 2024 4:18AM",
    actual_end: "20 Jan 2024 4:18AM",
    scheduled_start: "20 Jan 2024 4:18AM",
    scheduled_end: "20 Jan 2024 4:18AM",
    organization_id: 8833,
    organization_uuid: "c26tttvb-bvvvv22hh1jjj-fwwww-r333d",
    organization_name: "Optiv SAaas -LightBox",
  },
  {
    id: 3,
    serial: "g33rrhjh2hjhhkl22e2e9",
    analysis_status: "Completed- Results available",
    actual_start: "20 Jan 2024 4:18AM",
    actual_end: "20 Jan 2024 4:18AM",
    scheduled_start: "20 Jan 2024 4:18AM",
    scheduled_end: "20 Jan 2024 4:18AM",
    organization_id: 8834,
    organization_uuid: "c26tttvb-bvvvv22hh1jjj-fwwww-r333d",
    organization_name: "Optiv SAaas -LightBox",
  },
];

const medicines = [
  {
    "id": "1",
    "drug_code": "1",
    "generic_name": "Aceclofenac 100mg and Paracetamol 325 mg Tablet",
    "unit_size": "10's",
    "MRP": "8.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "2",
    "drug_code": "2",
    "generic_name": "Aceclofenac Tablets IP 100mg",
    "unit_size": "10's",
    "MRP": "4.37",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "3",
    "drug_code": "4",
    "generic_name": "Acetaminophen 325 + Tramadol Hydrochloride 37.5 film coated Tablet",
    "unit_size": "10's",
    "MRP": "8.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "4",
    "drug_code": "5",
    "generic_name": "ASPIRIN Tablets IP 150 mg",
    "unit_size": "14's",
    "MRP": "2.70",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "5",
    "drug_code": "9",
    "generic_name": "DICLOFENAC 50 mg+ PARACETAMOL 325 mg+ CHLORZOXAZONE 500 mg Tablets",
    "unit_size": "10's",
    "MRP": "11.30",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "6",
    "drug_code": "8",
    "generic_name": "Diclofenac Sodium 50mg + Serratiopeptidase 10mg Tablet",
    "unit_size": "10's",
    "MRP": "12.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "7",
    "drug_code": "9",
    "generic_name": "Diclofenac Sodium (SR) 100 mg Tablet",
    "unit_size": "10's",
    "MRP": "6.12",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "8",
    "drug_code": "10",
    "generic_name": "Diclofenac Sodium 25mg per ml Inj. IP",
    "unit_size": "3 ml",
    "MRP": "2.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "9",
    "drug_code": "11",
    "generic_name": "Diclofenac Sodium 50 mg Tablet",
    "unit_size": "10's",
    "MRP": "2.90",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "10",
    "drug_code": "12",
    "generic_name": "Etoricoxilb Tablets IP 120mg",
    "unit_size": "10's",
    "MRP": "33.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "11",
    "drug_code": "14",
    "generic_name": "Etoricoxilb Tablets IP 90mg",
    "unit_size": "10's",
    "MRP": "25.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "12",
    "drug_code": "14",
    "generic_name": "Ibuprofen 400 mg + Paracetamol 325 mg Tablet",
    "unit_size": "15's",
    "MRP": "5.50",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "",
    "drug_code": "",
    "generic_name": "Ibuprofen 200 mg film coated Tablet",
    "unit_size": "10's",
    "MRP": "1.80",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "",
    "drug_code": "16",
    "generic_name": "Ibuprofen 400 mg film coated Tablet",
    "unit_size": "15's",
    "MRP": "3.50",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "15",
    "drug_code": "20",
    "generic_name": "Nimesulide BP 100 mg Tablet",
    "unit_size": "10's",
    "MRP": "4.10",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "",
    "drug_code": "21",
    "generic_name": "Diclofenac Sodium 50 mg and Paracetamol 325 mg Tablet",
    "unit_size": "10's",
    "MRP": "6.87",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "17",
    "drug_code": "22",
    "generic_name": "Paracetamol Syrup IP 125mg/ 5ml",
    "unit_size": "60 ml bottles",
    "MRP": "10.60",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "18",
    "drug_code": "23",
    "generic_name": "Paracetamol Tablets IP 500mg",
    "unit_size": "10's",
    "MRP": "4.51",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "19",
    "drug_code": "24",
    "generic_name": "Pentazocine Injection IP 30mg/ ml",
    "unit_size": "1 ml",
    "MRP": "2.70",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "20",
    "drug_code": "25",
    "generic_name": "Serratiopeptidase 10 mg Tablet",
    "unit_size": "10's",
    "MRP": "8.90",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "21",
    "drug_code": "26",
    "generic_name": "Tramadol Hcl 100 mg Inj.",
    "unit_size": "2ml",
    "MRP": "6.32",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "22",
    "drug_code": "27",
    "generic_name": "Tramadol HCl Injection 50mg 1 ml",
    "unit_size": "1ml",
    "MRP": "4.30",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "23",
    "drug_code": "28",
    "generic_name": "Tramadol 50 mg Tablet",
    "unit_size": "10's",
    "MRP": "4.90",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "24",
    "drug_code": "336",
    "generic_name": "Allopurinol 100 mg Tablet",
    "unit_size": "10's",
    "MRP": "8.60",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "",
    "drug_code": "467",
    "generic_name": "DICYCLOMINE 10 mg+ MEFENAMIC ACID 250 mg Tablets",
    "unit_size": "10's",
    "MRP": "8.40",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "26",
    "drug_code": "509",
    "generic_name": "HYDROXYCHLOROQUINE Tablets IP 200 mg",
    "unit_size": "10's",
    "MRP": "30.00",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "",
    "drug_code": "510",
    "generic_name": "PARACETAMOL 325 mg+ TRAMADOL 37.5 mg Tablets",
    "unit_size": "10's",
    "MRP": "8.50",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "28",
    "drug_code": "511",
    "generic_name": "PARACETAMOL Tablets IP 650 mg",
    "unit_size": "15's",
    "MRP": "8.03",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "29",
    "drug_code": "512",
    "generic_name": "Aceclofenac 100 mg + Paracetamol 325 mg+ Serratiopeptidase 15 mg Tablets",
    "unit_size": "10's",
    "MRP": "12.60",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "30",
    "drug_code": "513",
    "generic_name": "PIROXICAM Capsules IP 20 mg",
    "unit_size": "10's",
    "MRP": "6.75",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "31",
    "drug_code": "515",
    "generic_name": "MEFENAMIC ACID SUSPENSION 100 mg/ 5 ml",
    "unit_size": "60 ML",
    "MRP": "14.92",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "32",
    "drug_code": "516",
    "generic_name": "ACECLOFENAC Tablets SR/ CR 200 mg",
    "unit_size": "10's",
    "MRP": "11.90",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  },
  {
    "id": "33",
    "drug_code": "517",
    "generic_name": "THIOCOLCHICOSIDE 4 mg+ ACECLOFENAC 100 mg Tablets",
    "unit_size": "10's",
    "MRP": "36.67",
    "therapeutic_category": "Analgesic \u0026 Antipyretic/ Muscle relaxants"
  }
  // {
  //   id: 1,
  //   name: "Paracetamol",
  //   price: "₹25",
  //   description: "",
  //   category: "",
  //   image: "https://upload.wikimedia.org/wikipedia/commons/b/b7/Paracetamol_generico.jpg",
  //   rating: {
  //     rate: 3.9,
  //     count: 120
  //   }
  // },
  // {
  //   id: 2,
  //   name: "Paracetamol 500mg",
  //   price: "₹30",
  //   description: "",
  //   category: "",
  //   image: "https://upload.wikimedia.org/wikipedia/commons/b/b7/Paracetamol_generico.jpg",
  //   rating: {
  //     rate: 3.9,
  //     count: 120
  //   }
  // }
];

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ extended: false, limit: "50mb" }));

app.get("/", (req, res) => {
  res.json({ status: true, message: "Test API" });
});

// Route to get all data
app.get("/api/posts", (req, res) => {
  res.json(posts);
});
app.get("/api/analysis", (req, res) => {
  res.json(analysis);
});
app.get("/api/medicines", (req, res) => {
  res.json(medicines);
});

// Route to get a single item by id
app.get("/api/posts/:id", (req, res) => {
  const { id } = req.params;
  const item = posts.find((item) => item.id === parseInt(id));
  if (item) {
    res.json(item);
  } else {
    res.status(404).send("Item not found");
  }
});
app.get("/api/analysis/:id", (req, res) => {
  const { id } = req.params;
  const item = analysis.find((item) => item.id === parseInt(id));
  if (item) {
    res.json(item);
  } else {
    res.status(404).send("Item not found");
  }
});

// Route to update an item by id
app.put("/api/posts/:id", (req, res) => {
  const { id } = req.params;
  const { review_status, sev, name, parameter, mitigation_status, status } =
    req.body;
  const index = posts.findIndex((item) => item.id === parseInt(id));
  const item = posts.find((item) => item.id === parseInt(id));

  if (index !== -1) {
    // Update the item
    posts[index] = {
      ...item,
      review_status,
      sev,
      name,
      parameter,
      mitigation_status,
      status,
    };
    res.json(posts[index]);
  } else {
    res.status(404).send("Item not found");
  }
});
// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
